<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="editt.css">
    <title>DSSV</title>
    
</head>

<?php
    $is_page_refreshed = (isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0');
    
    if (isset($_POST['select_khoa'])) {
        $_SESSION['select_khoa'] = $_POST['select_khoa'];
    }
    if (isset($_POST['key'])){
        $_SESSION['key'] = $_POST['key'];
    }
?>


<body>
    
    <form method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

        <div class="header">
            <div class="header1">

                <div class="item">

                    <div class="item_left">Khoa</div>

                    <div class="item_right" >
                        <select class="drop" name ='select_khoa' id="select_khoa">
                            <?php
                                $phankhoa = array(
                                    "MTA" => "Khoa học máy tính",
                                    "KDL" => "Khoa học dữ liệu"

                                );
                               
                                echo "<option value=''</option>";
                                foreach($phankhoa as $class => $value) {
                                    echo "<option value= '". $class . "'";
                                    if(array_key_exists('search',$_POST)) {
                                        if ($is_page_refreshed){
                                            if ($_SESSION['select_khoa'] === $class) {
                                                echo "selected";   
                                            }
                                        }
                                    }    
                                         
                                    echo ">$value</option>";
                                }
                                  
                            ?>   
                        </select>
                    </div>
                    
                </div>

                <div class="item">

                    <div class="item_left" >Từ khóa</div>

                    <div class="item_right">
                        <input type="text" class="drop key" name ="key" id ="text" 
                        <?php
                            if(array_key_exists("search",$_POST)) {
                                if ($is_page_refreshed){
                                    echo "value='" . $_SESSION["key"] ."'";
                                }
                            }
                        ?>
                        >
                        
                    </div>

                </div>
                <div class="item item_seach">
                    <button id="button" class="search delete">Xóa</button>
                    <button name="search"id ="search" class="search">Tìm kiếm</button>

                </div>



                <!-- clear các giá trị khoa và khóa -->
                

            </div>

            <div class="item left show_sv">Số sinh viên tìm thấy:</div>
            <div class="header2">
                
                <div class="item_left2">
                    <div class="item_left_2">
                        <div class="item_left_con1">No</div>
                        <div class="item_left_con">Tên sinh viên</div>
                        <div class="item_left_con">Khoa</div>
                    </div>

                    <div class="item_left_2">
                        <div class="item_left_con1">1</div>
                        <div class="item_left_con">Nguyễn Văn A</div>
                        <div class="item_left_con">Khoa học máy tính</div>
                    </div>
                    <div class="item_left_2">
                        <div class="item_left_con1">2</div>
                        <div class="item_left_con">Trần Thị B</div>
                        <div class="item_left_con">Khoa học máy tính</div>
                    </div>
                    <div class="item_left_2">
                        <div class="item_left_con1">3</div>
                        <div class="item_left_con">Nguyễn Hoàng C</div>
                        <div class="item_left_con">Khoa học vật liệu</div>
                    </div>
                    <div class="item_left_2">
                        <div class="item_left_con1">4</div>
                        <div class="item_left_con">Đinh Quang D</div>
                        <div class="item_left_con">Khoa học vật liệu</div>
                    </div>
                    
                </div>

                <div class="item_right2">
                    
                        <a href="submit.php" class="add">Thêm</a>
                    
                    <div class="action">Action</div>
                    <div class="item_right_con">
                        <div class="con_trai">Xóa</div>
                        <div class="con_trai">Sửa</div>
                    </div>
                    <div class="item_right_con">
                        <div class="con_trai">Xóa</div>
                        <div class="con_trai">Sửa</div>
                    </div>
                    <div class="item_right_con">
                        <div class="con_trai">Xóa</div>
                        <div class="con_trai">Sửa</div>
                    </div>
                    <div class="item_right_con">
                        <div class="con_trai">Xóa</div>
                        <div class="con_trai">Sửa</div>
                    </div>
                </div>

                
            </div>

        </div>
    
    </form>
    
    </body>
</html>